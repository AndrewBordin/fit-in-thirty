﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using System.IO;
using System.Runtime.Serialization;
using Windows.Storage;

namespace FitInThirty.BusinessLogic
{

    public enum Gender
    {
        Male = 1,
        Female
    }

    [DataContract]
    class Profile
    {
        [DataMember(Name ="UserName")]
        public string _name;

        [DataMember(Name ="UserAge")]
        public byte _age;

        [DataMember(Name ="UserWeight")]
        public float _weight;

        [DataMember(Name ="UserHeight")]
        public float _height;

        public ImageSource _img;

        private List<string> _list;

        [DataMember(Name ="UserDay")]
        private int _day;

        [DataMember(Name ="UserWeek")]
        private int _week;

        [DataMember(Name ="Bmi")]
        private double _bmi;

        public Profile()
        {
            _name = Name;
            _age = Age;
            _weight = Weight;
            _height = Height;
            _img = ImageUser;
            _bmi = BMI;
            _day = Day;
            _week = Week;
            _list = List;
            
        }

        public int Day
        {
            get { return _day; }
            set { _day = value; }
        }

        public int Week
        {
            get { return _week; }
            set { _week = value; }
        }

        public List<string> List
        {
            get { return _list; }
            set { _list = value; }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("No profile name has been set");
                }

                _name = value;
            }
        }

        public byte Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public float Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public float Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public double BMI
        {
            get { return _bmi; }
            set { _bmi = value; }
        }

        public ImageSource ImageUser
        {
            get { return _img; }
            set { _img = value; }
                 
        }

        public double CalculateBMI()
        {
            return _weight / Math.Pow((_height / 100), 2);
        }
    }
}
