﻿using FitInThirty.BusinessLogic;
using FitInThirty.Presentation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FitInThirty.Presentation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AccountCreationPage : Page
    {
        private Profile _profile;

        string profileFilePath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "ProfileData", "");
        public AccountCreationPage()
        {
            this.InitializeComponent();
            SystemNavigationManager navMgr = SystemNavigationManager.GetForCurrentView();

            //call the creation of the profile on initialization
            _profile = CreateProfile();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //obtain the bank object from the main page
            _profile = e.Parameter as Profile;
        }

        public async void OnSubmit(object sender, RoutedEventArgs e)
        {

            if(_chkFemale.IsChecked == false && _chkMale.IsChecked == false)
            {
                MessageDialog msgDlg = new MessageDialog("No gender has been checked! Please check a gender.","Missing Information");
                await msgDlg.ShowAsync();
            }

            if (_chkFemale.IsChecked == true || _chkMale.IsChecked == true)
            {
                //Generate everything once this is clicked and navigate the user to the dashboard
                try
                {
                    string name = _txtName.Text;
                    byte age = byte.Parse(_txtAge.Text);
                    float weight = float.Parse(_txtWeight.Text);
                    float height = float.Parse(_txtHeight.Text);

                    _profile.Name = name;
                    _profile.Age = age;
                    _profile.Height = height;
                    _profile.Weight = weight;
                    _profile.BMI = _profile.CalculateBMI();
                    _profile.List = new List<string>();

                    //the user has entered good details, save them now
                    Save();
                    //navigate the user to the dashboard
                    this.Frame.Navigate(typeof(DashboardPage), _profile);
                }
                catch (Exception)
                {
                    MessageDialog msgDlg = new MessageDialog("Please provide a name for the recipe", "Invalid information");
                    await msgDlg.ShowAsync();
                }
            }
        }


        //save the profile
        public void Save()
        {
            using (FileStream profileStream = new FileStream(profileFilePath, FileMode.Create))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Profile));
                serializer.WriteObject(profileStream, _profile);
            }
        }

        //return a new profile to the user
        private Profile CreateProfile()
        {
            return new Profile();
        }

        //checking whether the male or female gender is checked and switching the appearance
        //of the button once the other is checked
        private void OnMaleChecked(object sender, RoutedEventArgs e)
        {
            _chkMale.IsChecked = true;
            _chkFemale.IsChecked = false;
        }
        private void OnFemaleChecked(object sender, RoutedEventArgs e)
        {
            _chkFemale.IsChecked = true;
            _chkMale.IsChecked = false;
        }
    }
}
