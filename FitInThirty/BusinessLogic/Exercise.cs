﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitInThirty.BusinessLogic
{
    class Exercise
    {

        #region Field Variables
        private string _name;

        private int _defaultReps;

        private int _reps;

        private string _unitOfRep;

        private string _repDescription;

        private double _bmi; 
        #endregion   

        #region Properties
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int DefaultReps
        {
            get { return _defaultReps; }
            set { _defaultReps = value; }
        }

        public int Reps
        {
            get { return _reps; }
            set { _reps = value; }
        }

        public string UnitOfRep
        {
            get { return _unitOfRep; }
            set { _unitOfRep = value; }
        }

        public string RepDescription
        {
            get { return _repDescription; }
            set { _repDescription = value; }
        }
        #endregion

        public Exercise(string name, int defaultReps, string UnitOfRep, double bmi)
        {
            _name = name;

            _bmi = bmi;

            _defaultReps = defaultReps;

            _reps = 0;

            _unitOfRep = UnitOfRep;

            _repDescription = "";

            DefaultRepsBasedOnBMI();


        }

        #region Methods
        public void ChangeWeek(string week)
        {
            if (_unitOfRep == "" || _unitOfRep == "seconds")
            {
                if (week == "Week 1")
                {
                    _reps = _defaultReps;
                }

                else if (week == "Week 2")
                {
                    _reps = _defaultReps + 3;
                }

                else if (week == "Week 3")
                {
                    _reps = _defaultReps + 6;
                }

                else if (week == "Week 4")
                {
                    _reps = _defaultReps + 9;
                }
            }

            else
            {
                if (week == "Week 1")
                {
                    _reps = _defaultReps;
                }

                else if (week == "Week 2")
                {
                    _reps = _defaultReps + 1;
                }

                else if (week == "Week 3")
                {
                    _reps = _defaultReps + 2;
                }

                else if (week == "Week 4")
                {
                    _reps = _defaultReps + 3;
                }
            }

            UpdateRepDescription();
        }

        private void UpdateRepDescription()
        {
            if (_unitOfRep == "" || _unitOfRep == "minutes" || _unitOfRep == "seconds")
            {
                if (_unitOfRep == "minutes" && _reps == 1)
                {
                    _repDescription = $"3 Sets of {_reps} minute";
                }
                else
                {
                    _repDescription = $"3 Sets of {_reps} {_unitOfRep}";
                }
            }

            else
            {
                _repDescription = $"For {_reps} {_unitOfRep}";
            }
        }

        private void DefaultRepsBasedOnBMI()
        {
            if (_bmi < 18.5)
            {
                if (_unitOfRep == "" || _unitOfRep == "seconds")
                {
                    _defaultReps += 3;
                }

                else
                {
                    _defaultReps += 1;
                }
            }

            else if (_bmi >= 25)
            {
                if (_unitOfRep == "" || _unitOfRep == "seconds")
                {
                    _defaultReps -= 3;
                }

                else
                {
                    _defaultReps -= 1;
                }

            }
        }
    } 
    #endregion
}
