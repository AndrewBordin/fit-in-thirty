﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace FitInThirty.Presentation
{
    public class NavMenuItem
    {
        /// <summary>
        /// Automatic property for the navigation item label
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Automatic property for the symbol to be displayed next 
        /// to the label
        /// </summary>
        public Symbol Symbol { get; set; }

        /// <summary>
        /// Property to access the symbol as a character to
        /// be used in XAML
        /// </summary>
        public char SymbolAsChar
        {
            get { return (char)Symbol; }
        }

    }
}
