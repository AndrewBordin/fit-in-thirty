﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitInThirty.BusinessLogic
{
    /// <summary>
    /// Author: Fernando Costa
    /// Workout class that holds 5 different lists, each with 3 different exercises that is used to be displayed on
    /// the WorkoutPage.
    /// </summary>
    class Workout
    {
        #region Field Variables
        //field variables lists that hold exercises for each category stated in the name
        private List<Exercise> _chestAbs;

        private List<Exercise> _legs;

        private List<Exercise> _arms;

        private List<Exercise> _back;

        private List<Exercise> _cardio;

        //bmi field variable that will be assigned to whatever is passed to the workout constructor
        private double _bmi;

        #endregion

        #region Properties
        public List<Exercise> ChestAbs
        {
            get { return _chestAbs; }
        }

        public List<Exercise> Legs
        {
            get { return _legs; }
        }

        public List<Exercise> Arms
        {
            get { return _arms; }
        }

        public List<Exercise> Back
        {
            get { return _back; }
        }

        public List<Exercise> Cardio
        {
            get { return _cardio; }
        }

        public double Bmi
        {
            get { return _bmi; }

            set { _bmi = value; }
        } 
        #endregion

        /// <summary>
        /// Constructor for the Workout class
        /// </summary>
        public Workout(double bmi)
        {
            _chestAbs = new List<Exercise>();
            _legs = new List<Exercise>();
            _arms = new List<Exercise>();
            _back = new List<Exercise>();
            _cardio = new List<Exercise>();

            _bmi = bmi;
        }

        /// <summary>
        /// Method that creates and initializes exercises field variables and adds them to the appropriate list
        /// </summary>
        public void CreateExercises()
        {
            Exercise sitUps = new Exercise("Sit Ups", 12, "", _bmi);
            Exercise pushUps = new Exercise("Push Ups", 12, "", _bmi);
            Exercise plank = new Exercise("Plank", 2, "minutes", _bmi);

            _chestAbs.Add(sitUps);
            _chestAbs.Add(pushUps);
            _chestAbs.Add(plank);

            Exercise squats = new Exercise("Squats", 15, "", _bmi);
            Exercise calfRaises = new Exercise("Calf Raises", 20, "", _bmi);
            Exercise wallSit = new Exercise("Wall Sit", 3, "minutes", _bmi);

            _legs.Add(squats);
            _legs.Add(calfRaises);
            _legs.Add(wallSit);

            Exercise dips = new Exercise("Dips", 12, "", _bmi);
            Exercise pullUps = new Exercise("Pull Ups", 12, "", _bmi);
            Exercise diamondPushUps = new Exercise("Diamond Pull Ups", 12, "", _bmi);

            _arms.Add(dips);
            _arms.Add(pullUps);
            _arms.Add(diamondPushUps);

            Exercise superman = new Exercise("Superman", 20, "", _bmi);
            Exercise sidePlank = new Exercise("Side Plank", 20, "seconds", _bmi);
            Exercise armNLegRaises = new Exercise("Opposite Arm and Leg Raises", 12, "", _bmi);

            _back.Add(superman);
            _back.Add(sidePlank);
            _back.Add(armNLegRaises);

            Exercise walk = new Exercise("Walk", 2, "km", _bmi);
            Exercise jog = new Exercise("Jog", 3, "km", _bmi);

            _cardio.Add(walk);
            _cardio.Add(jog);
        }
    }
}
