﻿using FitInThirty.BusinessLogic;
using FitInThirty.Presentation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FitInThirty
{
    //Testing Commits
    //Testing Commits-Fernando
    //test, test, 1, 2 ,3
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Profile _profile;

        private Workout _workout;

        private Fit _fit;

        private bool profileCreated = true;

        public MainPage()
        {
            this.InitializeComponent();

            _profile = new Profile();

            Load();

            if(profileCreated == false)
            {
                _frmContent.Navigate(typeof(AccountCreationPage), _profile);
            }
            else
            {
                _frmContent.Navigate(typeof(DashboardPage), _profile);
            }
            
            SystemNavigationManager navMgr = SystemNavigationManager.GetForCurrentView();
            navMgr.BackRequested += OnNavigateBack;

            //PC customization
            if (ApiInformation.IsTypePresent("Windows.UI.ViewManagement.ApplicationView"))
            {
                var titleBar = ApplicationView.GetForCurrentView().TitleBar;
                if (titleBar != null)
                {
                    titleBar.ButtonBackgroundColor = Colors.IndianRed;
                    titleBar.ButtonForegroundColor = Colors.Black;
                    titleBar.BackgroundColor = Colors.IndianRed;
                    titleBar.ForegroundColor = Colors.Black;
                }
            }
        }

        private void Load()
        {
            try
            {
                string FileName = Path.GetDirectoryName(Path.Combine(ApplicationData.Current.LocalFolder.Path, "ProfileData", "ProfileData"));
                using (FileStream acctStream = new FileStream(FileName, FileMode.Open))
                {
                    using (XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(acctStream, new XmlDictionaryReaderQuotas()))
                    {
                        DataContractSerializer serializer = new DataContractSerializer(typeof(Profile));
                        Profile prof = serializer.ReadObject(reader, true) as Profile;

                        _profile = prof;
                    }
                }
            }
            catch (Exception)
            {
                profileCreated = false;
            }

            try
            {
                string FileName2 = Path.GetDirectoryName(Path.Combine(ApplicationData.Current.LocalFolder.Path, "WorkoutDayList", "WorkoutDayList"));
                using (FileStream acctStream = new FileStream(FileName2, FileMode.Open))
                {
                    using (XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(acctStream, new XmlDictionaryReaderQuotas()))
                    {
                        DataContractSerializer serializer = new DataContractSerializer(typeof(List<string>));
                        List<string> list = serializer.ReadObject(reader, true) as List<string>;


                        _profile.List = list;
                    }
                }
            }
            catch (Exception)
            {

            }
            

        }

        private void OnNavigateBack(object sender, BackRequestedEventArgs e)
        {
            if (_frmContent.CanGoBack)
            {
                _frmContent.GoBack();
                e.Handled = true;
            }
        }

        private async void OnNavigationItemClick(object sender, ItemClickEventArgs e)
        {
           

                NavMenuItem clickedMenuItem = e.ClickedItem as NavMenuItem;
                if (clickedMenuItem == _uiNavHome)
                {
                _navSplitView.IsPaneOpen = false;
                    _frmContent.Navigate(typeof(DashboardPage), _profile);
                }
                else if (clickedMenuItem == _uiNavWorkout)
                {
                    //close the pane before navigating to the next page if the split view is in overlay mode. 
                    //Otherwise, the Back button will first need to close the pane. When the pane is not visible on
                    //the destination page, it looks like the first Back button is ignored.
                    if (_navSplitView.DisplayMode == SplitViewDisplayMode.Overlay || _navSplitView.DisplayMode == SplitViewDisplayMode.CompactOverlay)
                    {
                        _navSplitView.IsPaneOpen = false;
                    }

                    _frmContent.Navigate(typeof(WorkoutPage), _profile);
                }
                else if (clickedMenuItem == _uiNavHowTo)
                {
                    if (_navSplitView.DisplayMode == SplitViewDisplayMode.Overlay || _navSplitView.DisplayMode == SplitViewDisplayMode.CompactOverlay)
                    {
                        _navSplitView.IsPaneOpen = false;
                    }

                    _frmContent.Navigate(typeof(HowToPage), _fit);

                }
                else if (clickedMenuItem == _uiNavRecTrans)
                {
                    MessageDialog msgDlg = new MessageDialog("TODO: navigate to the transaction page", "Record Transaction");
                    await msgDlg.ShowAsync();

                }
                else
                {
                    Debug.Assert(false, "Unknown menu item");
                }

            
        }

        private void OnContentFrameNavigated(object sender, NavigationEventArgs e)
        {
            _lstAppNavigation.IsEnabled = true;
            _txtWelcomeMessage.Text = "";
            if (e.SourcePageType == typeof(DashboardPage))
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
                _lstAppNavigation.SelectedItem = _uiNavHome;
                _txtPageTitle.Text = "Dashboard";
                _txtWelcomeMessage.Text = "Welcome, " + _profile.Name;
            }
            else if (e.SourcePageType == typeof(WorkoutPage))
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
                _lstAppNavigation.SelectedItem = _uiNavWorkout;
                _txtPageTitle.Text = "Workout";
            }
            else if(e.SourcePageType == typeof(HowToPage))
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
                _lstAppNavigation.SelectedItem = _uiNavHowTo;
                _txtPageTitle.Text = "How-to";
            }
            else if (e.SourcePageType == typeof(AccountCreationPage))
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
                _txtPageTitle.Text = "F.I.T";
                _lstAppNavigation.IsEnabled = false;
                
            }
        }
    }
}
