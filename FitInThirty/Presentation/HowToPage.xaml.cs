﻿using FitInThirty.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FitInThirty.Presentation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HowToPage : Page
    {
        //field variables
        private HowTo _howto;
        private int varChest;
        private int varLegs;
        private int varArms;
        private int varBack;
        private int varCardio;
        private int checker;

        public HowToPage()
        {
            this.InitializeComponent();

            _howto = new HowTo();
            _howto.CreateDescriptions();
        }

        #region Methods
        /// <summary>
        /// the control when one of the 5 buttons are clicked
        /// activates checker, resets the image/textblock and
        /// displays the workouts to the listview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickBtn(object sender, RoutedEventArgs e)
        {
            if (sender == _btnChest)
            {
                checker = 0;
                _txtWorkoutDescription.Text = "PLEASE SELECT A WORKOUT TO LEARN MORE";
                DisplayBlankImage(imgWorkout);
                DisplayChestWorkouts();
            }
            else if (sender == _btnLegs)
            {
                checker = 1;
                _txtWorkoutDescription.Text = "PLEASE SELECT A WORKOUT TO LEARN MORE";
                DisplayBlankImage(imgWorkout);
                DisplayLegsWorkouts();
            }
            else if (sender == _btnArms)
            {
                checker = 2;
                _txtWorkoutDescription.Text = "PLEASE SELECT A WORKOUT TO LEARN MORE";
                DisplayBlankImage(imgWorkout);
                DisplayArmsWorkouts();
            }
            else if (sender == _btnBack)
            {
                checker = 3;
                _txtWorkoutDescription.Text = "PLEASE SELECT A WORKOUT TO LEARN MORE";
                DisplayBlankImage(imgWorkout);
                DisplayBackWorkouts();
            }
            else if (sender == _btnCardio)
            {
                checker = 4;
                _txtWorkoutDescription.Text = "PLEASE SELECT A WORKOUT TO LEARN MORE";
                DisplayBlankImage(imgWorkout);
                DisplayCardioWorkouts();
            }
            else
            {
                Debug.Assert(false, "Invalid button selected!");
            }
        }

        /// <summary>
        /// this method puts the blank image to _imgWorkout
        /// </summary>
        /// <param name="imageControl"></param>
        private void DisplayBlankImage(Image imageControl)
        {
            string imgAssetPath = $"ms-appx:///Assets/HowToAssets/howtobg.png";
            imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
        }

        #region DisplayWorkouts
        /// <summary>
        /// all the following methods display the workouts
        /// from the list that corresponds to the body part
        /// selected to workout
        /// </summary>
        private void DisplayChestWorkouts()
        {
            _lstWorkouts.Items.Clear();

            foreach (Description desc in _howto.Chest)
            {
                _lstWorkouts.Items.Add(desc);
            }
        }

        private void DisplayLegsWorkouts()
        {
            _lstWorkouts.Items.Clear();

            foreach (Description desc in _howto.Legs)
            {
                _lstWorkouts.Items.Add(desc);
            }
        }

        private void DisplayArmsWorkouts()
        {
            _lstWorkouts.Items.Clear();

            foreach (Description desc in _howto.Arms)
            {
                _lstWorkouts.Items.Add(desc);
            }
        }
        private void DisplayBackWorkouts()
        {
            _lstWorkouts.Items.Clear();

            foreach (Description desc in _howto.Back)
            {
                _lstWorkouts.Items.Add(desc);
            }
        }

        private void DisplayCardioWorkouts()
        {
            _lstWorkouts.Items.Clear();

            foreach (Description desc in _howto.Cardio)
            {
                _lstWorkouts.Items.Add(desc);
            }
        }
        #endregion

        #region DisplayDescriptions
        /// <summary>
        /// this is the action for the SelectionChanged action for _lstWorkouts
        /// displays the description that explains how to do the workout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectWorkout(object sender, SelectionChangedEventArgs e)
        {
            Description selectedWorkout = _lstWorkouts.SelectedItem as Description;

            if (checker == 0)
            {
                varChest = _lstWorkouts.SelectedIndex;
                if (varChest == 0)
                {
                    _txtWorkoutDescription.Text = 
                        "Lie on your back. Bend your knees and plant your feet about hip-distance apart. Place your hands on the back of your head, where it attaches to your neck. Point your elbows to the sides of the room. " +
                        "Exhale and pull your belly button in toward your spine as you gently raise your torso by bending your hips and waist. Lift up until your torso is just inches from your thighs. " +
                        "Inhale and control your return to the start position to complete one repetition.";
                    ShowWorkoutImg(imgWorkout);
                }
                else if (varChest == 1)
                {
                    _txtWorkoutDescription.Text =
                        "Place your hands firmly on the ground, directly under shoulders. Ground your toes into the floor to stabilize your lower half and flatten your back so your entire body is neutral and straight. " +
                        "Begin to lower your body keeping your back flat and eyes focused about three feet in front of you to keep a neutral neck. Don’t let your butt dip or stick out at any point, your body should remain in a straight line from head to toe. Draw shoulder blades back and down, keeping elbows tucked close to your body. " +
                        "Exhale as you push back to the starting position.";
                    ShowWorkoutImg(imgWorkout);
                }
                else if (varChest == 2)
                {
                    _txtWorkoutDescription.Text =
                        "Place the forearms on the ground with the elbows aligned below the shoulders, and arms parallel to the body at about shoulder-width distance. " +
                        "Ground the toes into the floor and squeeze the glutes to stabilize the body. Your legs should be working in the move too; careful not to lock or hyperextend your knees." +
                        "Your head should be in line with your back." +
                        "As you get more comfortable with the move, hold your plank for as long as possible without compromising form or breath.";
                    ShowWorkoutImg(imgWorkout);
                }
            }
            else if (checker == 1)
            {
                varLegs = _lstWorkouts.SelectedIndex;
                if (varLegs == 0)
                {
                    _txtWorkoutDescription.Text =
                        "Stand with your head facing forward and your chest held up and out. " +
                        "Place your feet shoulder-width apart or slightly wider. Extend your hands straight out in front of you to help keep your balance. " +
                        "Sit back and down like you're sitting into an imaginary chair. Keep your head facing forward as your upper body bends forward a bit and let your lower back arch slightly as you descend. " +
                        "Lower down so your thighs are as parallel to the floor as possible, with your knees over your ankles. " +
                        "Push through your heels to bring yourself back to the starting position.";

                    ShowWorkoutImg(imgWorkout);
                }
                else if (varLegs == 1)
                {
                    _txtWorkoutDescription.Text = 
                        "Stand on the edge of a step or any platform available. " +
                        "Stand tall with your abdominals pulled in, the balls of your feet firmly planted on the step, and your heels hanging over the edge. " +
                        "Raise your heels a few inches above the edge of the step so that you’re on your tiptoes. " +
                        "Hold the position for a moment, and then lower your heels below the platform, feeling a stretch in your calf muscles.";

                    ShowWorkoutImg(imgWorkout);
                }
                else if (varLegs == 2)
                {
                    _txtWorkoutDescription.Text = 
                        "Slide your back down the wall until your hips and knees bend at a 90 degrees angle. " + 
                        "Place your arms out from and keep the shoulders, upper back and the back of the head against the wall. " + 
                        "Both feet should be flat on the ground with the weight evenly distributed. Hold for the required amount of time.";

                    ShowWorkoutImg(imgWorkout);
                }
            }
            else if (checker == 2)
            {
                varArms = _lstWorkouts.SelectedIndex;
                if (varArms == 0)
                {
                    _txtWorkoutDescription.Text =
                        "Hoist yourself up onto a bench, chair or step as long as it is stable and secure to take your body weight. " +
                        "Your hands should be shoulder width apart fingers facing forward and elbows pointing backwards with a slight bend in the elbows. " + 
                        "Then, slowy lower your body using by bending your elbows and inhale on the way down. " + 
                        "Push yourself back up to the starting position as you exhale.";


                    ShowWorkoutImg(imgWorkout);
                }
                else if (varArms == 1)
                {
                    _txtWorkoutDescription.Text = 
                        "Grip the pullup bar from a dead-hang position, palms facing away from you. Pull your body up toward the bar in a slow and controlled movement. " +
                        "Pause for a second as your chin gets close the bar and then slowly lower yourself to the starting position. ";

                    ShowWorkoutImg(imgWorkout);
                }
                else if (varArms == 2)
                {
                    _txtWorkoutDescription.Text =
                        "Get into the standard pushup position with your hands together directly beneath your chest so that index fingers and thumbs are touching to form a triangle or 'diamond' shape" +
                        "Begin to lower your body keeping your back flat and eyes focused about three feet in front of you to keep a neutral neck. Don’t let your butt dip or stick out at any point, your body should remain in a straight line from head to toe. " +
                        "Exhale as you push back to the starting position."; ;

                    ShowWorkoutImg(imgWorkout);
                }
            }
            else if (checker == 3)
            {
                varBack = _lstWorkouts.SelectedIndex;
                if (varBack == 0)
                {
                    _txtWorkoutDescription.Text = 
                        "Begin by laying down flat on your stomach with your arms out front and legs straight. " + 
                        "Then you Simultaneously raise your arms, legs, and chest off of the floor and hold this contraction for 2 seconds. Make sure you squeeze your lower back to get the best results from this exercise. Remember to exhale during this movement.";

                    ShowWorkoutImg(imgWorkout);

                }
                else if (varBack == 1)
                {
                    _txtWorkoutDescription.Text =
                        "Start on your side with your feet together and one forearm directly below your shoulder. " +
                        "Contract your core and raise your hips until your body is in a straight line from head to feet. " +
                        "Hold the position without letting your hips drop for the allotted time for each set, then repeat on the other side.";

                    ShowWorkoutImg(imgWorkout);
                }
                else if (varBack == 2)
                {
                    _txtWorkoutDescription.Text = 
                        "To work your six-pack area and back, remain on all fours and tighten your abdominal muscles, keeping your spine and neck in a neutral position; " + 
                        "you should be looking at the floor (top illustration). Slowly extend your left leg behind you while reaching your right arm forward (bottom illustration). " + 
                        "Keep your hips and shoulders square and make sure your lower back doesn’t arch. Hold for five seconds. Slowly return to the starting position and do the move on the opposite side.";

                    ShowWorkoutImg(imgWorkout);
                }
            }
            else if (checker == 4)
            {
                varCardio = _lstWorkouts.SelectedIndex;
                if (varCardio == 0)
                {
                    _txtWorkoutDescription.Text = 
                        "Before you begin your walk or jog, it is essential that you stretch first. This will allow your muscles to be more relaxed during your exercise. " +
                        "Don't just hop stright into a sprint or fast walk, start at a slow and steady pace to allow your body to warm up. " + 
                        "Make sure that you breathe at a good pace as well so that you can go a futher distance without getting too tired. ";

                    ShowWorkoutImg(imgWorkout);
                }
            }
            else
            {
                Debug.Assert(false, "Invalid workout selected! Please try again! ");
            }
        }
        #endregion

        #region DisplayImages
        /// <summary>
        /// ShowWorkoutImg displays the image of the workout selected
        /// uses the checker and index of the listview to display
        /// the correct image
        /// </summary>
        /// <param name="imageControl"></param>
        private void ShowWorkoutImg(Image imageControl)
        {
            if (checker == 0)
            {
                if (varChest == 0)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/c01.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
                else if (varChest == 1)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/c02.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
                else if (varChest == 2)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/c03.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
            }
            else if (checker == 1)
            {
                if (varLegs == 0)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/l01.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
                else if (varLegs == 1)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/l02.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
                else if (varLegs == 2)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/l03.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
            }

            else if (checker == 2)
            {
                if (varArms == 0)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/a01.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
                else if (varArms == 1)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/a02.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
                else if (varArms == 2)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/a03.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
            }
            else if (checker == 3)
            {
                if (varBack == 0)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/b01.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));

                }
                else if (varBack == 1)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/b02.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
                else if (varBack == 2)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/b03.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
            }
            else if (checker == 4)
            {
                if (varCardio == 0)
                {
                    string imgAssetPath = $"ms-appx:///Assets/HowToAssets/r01.png";
                    imageControl.Source = new BitmapImage(new Uri(imgAssetPath));
                }
            }
            else
            {
                Debug.Assert(false, "Invalid workout selected!");
            }
            #endregion

            #endregion

        }
    }
}
