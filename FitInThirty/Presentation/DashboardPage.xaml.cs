﻿using FitInThirty.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Streams;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FitInThirty.Presentation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary
    public sealed partial class DashboardPage : Page
    {
        private Profile _profile;

        private Workout _workout;

        BitmapImage image = new BitmapImage();

        //variables for all the goals so that they can be increased and decreased
        public int weightGoal;
        private int stepGoal;
        private int fatGoal;
        private int weekGoal;

        public DashboardPage()
        {
            this.InitializeComponent();
            _profile = null;
            _workout = null;

            weightGoal = int.Parse(_txtWeightGoal.Text);
            stepGoal = int.Parse(_txtStepsGoal.Text);
            fatGoal = int.Parse(_txtFatGoal.Text);
            weekGoal = int.Parse(_txtWeeksGoal.Text);
            LoadImageAvatar();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _profile = e.Parameter as Profile;
            _workout = e.Parameter as Workout;
            //  Week_Day = e.Parameter as List<T>
            DisplayDetails();

            try {
                if (_profile.Week >= 5)
                {
                    _txtCurrentDay.Text = "Workouts All Done!!";
                }
                else
                {
                    _txtCurrentDay.Text = _profile.List[_profile.List.Count - 1];
                }
            }

            catch (Exception)
            {
                _txtCurrentDay.Text = "Week 1, Day 1";
            }            
        }

        private void DisplayDetails()
        {
            //Setting the text next to the users image to their own details
            _txtProfileName.Text = _profile.Name;
            _txtProfileAge.Text = "Age: " + (_profile.Age).ToString();
            _txtProfileWeight.Text ="Weight: " + (_profile.Weight).ToString() + "kg";
            _txtProfileHeight.Text ="Height: " + (_profile.Height).ToString() + "cm";

            if(_profile._img != null)
            {
                _imgUser.ImageSource = _profile._img;
            }

            //TODO: get list of days and weeks from workout page
           
        }

        private void UpcomingWorkouts()
        {
           //TODO: Display the workouts from the workout object and display upcoming workouts in the list view
        }


        private async void OnEditImage(object sender, RoutedEventArgs e)
        {
            //ask the user to provide the app with a file
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");
            openPicker.FileTypeFilter.Add(".bmp");
            StorageFile file = await openPicker.PickSingleFileAsync();

            //store the avatar image file to application data storage
            StorageFile avatarImageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("AvatarImage", CreationCollisionOption.ReplaceExisting);
            await file.CopyAndReplaceAsync(avatarImageFile);

            //display the new image as the dancing avatar
            LoadImageAvatar();
        }

        private async void LoadImageAvatar()
        {

            try
            {
                //obtain the storage file for the avatar image to use 
                StorageFile avatarImageFile = await ApplicationData.Current.LocalFolder.GetFileAsync("AvatarImage");

                //obtain a stream for reading the image data
                using (IRandomAccessStream imgStream = await avatarImageFile.OpenAsync(FileAccessMode.Read))
                {
                    BitmapImage image = new BitmapImage();
                    image.SetSourceAsync(imgStream);
                    _imgUser.ImageSource = image;
                }
            }
            catch (FileNotFoundException)
            {
                //obtain the file from the app assets and put the file in local storage
                StorageFile avatarImgAssetFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/noImage.png"));

                //copy the file into local storage
                avatarImgAssetFile.CopyAsync(ApplicationData.Current.LocalFolder);

                //display the asset avatar as the image source
                _imgUser.ImageSource = new BitmapImage()
                {
                    UriSource = new Uri("ms-appx:///Assets/noImage.png")
                };
            }
            catch (IOException)
            {
                _imgUser.ImageSource = new BitmapImage()
                {
                    UriSource = new Uri("ms-appx:///Assets/noImage.png")
                };
            }

        }

        private void OnNavWorkoutfrmDash(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(WorkoutPage), _profile);
        }

        private void OnNavHowtofrmDash(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(HowToPage));
        }

        //Changing the goal based on the buttons
        private void OnChangeGoal(object sender, RoutedEventArgs e)
        {

            if (sender == _btnWeightUp)
            {
                weightGoal++;
            }
            else if(sender == _btnWeightDown)
            {
                weightGoal--;
            }
            else if(sender == _btnStepsUp)
            {
                stepGoal++;
            }
            else if (sender == _btnStepsDown)
            {
                stepGoal--;
            }
            else if (sender == _btnFatUp)
            {
                fatGoal++;
            }
            else if (sender == _btnFatDown)
            {
                fatGoal--;
            }
            else if (sender == _btnWeeksUp)
            {
                weekGoal++;
            }
            else if (sender == _btnWeeksDown)
            {
                weekGoal--;
            }

            //once done, set the text to the integer value
            _txtWeightGoal.Text = weightGoal.ToString();
            _txtStepsGoal.Text = stepGoal.ToString();
            _txtFatGoal.Text = fatGoal.ToString();
            _txtWeeksGoal.Text = weekGoal.ToString();
        
        }
    }
}
