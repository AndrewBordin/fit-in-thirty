﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitInThirty.BusinessLogic
{
    class Description
    {
        //field variable
        private string _workoutName;

        //initialize field variable
        public Description(string workoutName)
        {
            _workoutName = workoutName;
        }

        //property
        public string WorkoutName
        {
            get { return _workoutName; }
        }

        //this method allows the workout name to be
        //displayed in the listview
        public override string ToString()
        {
            return _workoutName;
        }
    }
}
