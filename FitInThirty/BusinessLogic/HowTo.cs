﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitInThirty.BusinessLogic
{
    class HowTo
    {
        //chest list created
        private List<Description> _chest;

        //legs list created
        private List<Description> _legs;

        //arms list created
        private List<Description> _arms;

        //back list created
        private List<Description> _back;

        //cardio list created
        private List<Description> _cardio;

        #region Properties

        public List<Description> Chest
        {
            get { return _chest; }
        }

        public List<Description> Legs
        {
            get { return _legs; }
        }
        public List<Description> Arms
        {
            get { return _arms; }
        }
        public List<Description> Back
        {
            get { return _back; }
        }
        public List<Description> Cardio
        {
            get { return _cardio; }
        }

        #endregion

        /// <summary>
        /// constructor for the HowTo page
        /// initializes the Lists
        /// </summary>
        public HowTo()
        {
            _chest = new List<Description>();

            _legs = new List<Description>();

            _arms = new List<Description>();

            _back = new List<Description>();

            _cardio = new List<Description>();
        }

        /// <summary>
        /// this method creates the names of the workouts
        /// and adds them to the correct list
        /// </summary>
        public void CreateDescriptions()
        {
            //creating workout names (everything below)
            Description sitUps = new Description("Sit Ups");
            Description pushUps = new Description("Push Ups");
            Description plank = new Description("Plank");

            //adding workouts to chest list
            _chest.Add(sitUps);
            _chest.Add(pushUps);
            _chest.Add(plank);

            Description squats = new Description("Squats");
            Description calfRaise = new Description("Calf Raises");
            Description wallSit = new Description("Wall Sit");

            //adding workouts to legs list
            _legs.Add(squats);
            _legs.Add(calfRaise);
            _legs.Add(wallSit);

            Description dips = new Description("Dips");
            Description pullUps = new Description("Pull Ups");
            Description diamondPushups = new Description("Diamond Pushups");

            //adding workouts to arms list
            _arms.Add(dips);
            _arms.Add(pullUps);
            _arms.Add(diamondPushups);

            Description superman = new Description("Superman");
            Description sidePlank = new Description("Side Plank");
            Description birdDog = new Description("Bird Dog");

            //adding workouts to back list
            _back.Add(superman);
            _back.Add(sidePlank);
            _back.Add(birdDog);

            Description cardio = new Description("Cardio");

            //adding workouts to cardio list
            _cardio.Add(cardio);

        }
    }
}
