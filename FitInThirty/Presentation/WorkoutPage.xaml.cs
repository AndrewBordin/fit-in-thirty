﻿using FitInThirty.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Xml;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FitInThirty.Presentation
{
    /// <summary>
    /// Author: Fernando Costa
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class WorkoutPage : Page
    {
        #region Field Variables
        //Workout object to be initialized that will be worked with
        private Workout _workout;

        //Day the user selects (1 to 5)
        private int _daySelected;

        //Week user selects (1-4)
        private int _weekSelected;

        //The profile object to be worked with
        private Profile _profile;

        //A list with different motivational quotes to be displayed on the page
        private List<string> _motivationList;

        //A random object to generate random int to call random quotes from the _motivationList
        private Random _random;

        //Data persistence list that contains the week and day user completed
        string profileFilePath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "WorkoutDayList", "");

        //Data persistence for the profile page and its data, which this page updates the _day and _week found in profile
        string profileFilePath2 = Path.Combine(ApplicationData.Current.LocalFolder.Path, "ProfileData", ""); 
        #endregion

        public WorkoutPage()
        {
            this.InitializeComponent();

            _random = new Random();

            //Add motivation to the motivation list
            _motivationList = new List<string>();
            _motivationList.Add("You Rock!");
            _motivationList.Add("Keep on going, c'mon!");
            _motivationList.Add("You can do this :)");
            _motivationList.Add("Keep up the good work");
            _motivationList.Add("Reward yourself after you're done the workout");
            _motivationList.Add("Don't ever forget to stretch!");
            _motivationList.Add("Don't ever forget to eat properly based on your goals!");

            //Fill out the different combo boxes with the options for selection
            //Week 1, Week 2, Week 3 and Week 4
            FillComboBox(_comboBoxChest);
            FillComboBox(_comboBoxLegs);
            FillComboBox(_comboBoxArms);
            FillComboBox(_comboBoxBack);
            FillComboBox(_comboBoxCardio);

            //Profile object and workout object that will be sent to this page when navigated to
            _profile = null;
            _workout = null;

            //Week and day selected on the page by the user
            _weekSelected = 0;
            _daySelected = 0;

        }

        #region Methods
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _profile = e.Parameter as Profile;
            _txtBlockMotivation.Text = _motivationList[_random.Next(7)];

            if (_workout == null)
            {
                _workout = new Workout(_profile.BMI);
                _workout.CreateExercises();
            }
        }

        /// <summary>
        /// Fills out ComboBoxes on WorkoutPage with appropriate options
        /// </summary>
        /// <param name="comboBox"></param>
        private void FillComboBox(ComboBox comboBox)
        {
            comboBox.Items.Insert(0, "Select Week");
            comboBox.Items.Insert(1, "Week 1");
            comboBox.Items.Insert(2, "Week 2");
            comboBox.Items.Insert(3, "Week 3");
            comboBox.Items.Insert(4, "Week 4");
            comboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Tells the program what to do when the user makes a selection on the combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox choice = (ComboBox)sender;

            if (choice.SelectedIndex != 0)
            {
                if (choice == _comboBoxChest)
                {
                    _txtBlockSelectedChoice.Text = "Selected Option is " + _txtBlockChest.Text + " -> " + WeekSelected(choice, 1);
                    AddChestWorkoutsListView(WeekSelected(choice, 1));
                }

                else if (choice == _comboBoxLegs)
                {
                    _txtBlockSelectedChoice.Text = "Selected Option is " + _txtBlockLegs.Text + " -> " + WeekSelected(choice, 2);
                    AddLegsWorkoutsListView(WeekSelected(choice, 2));
                }

                else if (choice == _comboBoxArms)
                {
                    _txtBlockSelectedChoice.Text = "Selected Option is " + _txtBlockArms.Text + " -> " + WeekSelected(choice, 3);
                    AddArmsWorkoutsListView(WeekSelected(choice, 3));
                }

                else if (choice == _comboBoxBack)
                {
                    _txtBlockSelectedChoice.Text = "Selected Option is " + _txtBlockBack.Text + " -> " + WeekSelected(choice, 4);
                    AddBackWorkoutsListView(WeekSelected(choice, 4));
                }

                else if (choice == _comboBoxCardio)
                {
                    _txtBlockSelectedChoice.Text = "Selected Option is " + _txtBlockCardio.Text + " -> " + WeekSelected(choice, 5);
                    AddCardioWorkoutsListView(WeekSelected(choice, 5));
                }

            }

            else
            {
                _txtBlockSelectedChoice.Text = "A list with the workouts and reps will appear below once you make a seleciton";
                _listViewWorkouts.Items.Clear();
            }
        }

        private void AddCardioWorkoutsListView(string week)
        {
            if (week == "Week 1")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Cardio)
                {
                    exercise.ChangeWeek("Week 1");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 2")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Cardio)
                {
                    exercise.ChangeWeek("Week 2");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 3")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Cardio)
                {
                    exercise.ChangeWeek("Week 3");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 4")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Cardio)
                {
                    exercise.ChangeWeek("Week 4");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }
        }

        //The following methods will add the appropriate workout to the listview, with the appropriate exercises
        //and reps for that week
        private void AddBackWorkoutsListView(string week)
        {
            if (week == "Week 1")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Back)
                {
                    exercise.ChangeWeek("Week 1");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 2")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Back)
                {
                    exercise.ChangeWeek("Week 2");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 3")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Back)
                {
                    exercise.ChangeWeek("Week 3");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 4")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Back)
                {
                    exercise.ChangeWeek("Week 4");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }
        }

        private void AddArmsWorkoutsListView(string week)
        {
            if (week == "Week 1")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Arms)
                {
                    exercise.ChangeWeek("Week 1");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 2")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Arms)
                {
                    exercise.ChangeWeek("Week 2");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 3")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Arms)
                {
                    exercise.ChangeWeek("Week 3");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 4")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Arms)
                {
                    exercise.ChangeWeek("Week 4");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }
        }

        private void AddLegsWorkoutsListView(string week)
        {
            if (week == "Week 1")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Legs)
                {
                    exercise.ChangeWeek("Week 1");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 2")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Legs)
                {
                    exercise.ChangeWeek("Week 2");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 3")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Legs)
                {
                    exercise.ChangeWeek("Week 3");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 4")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.Legs)
                {
                    exercise.ChangeWeek("Week 4");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }
        }

        private void AddChestWorkoutsListView(string week)
        {
            if (week == "Week 1")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.ChestAbs)
                {
                    exercise.ChangeWeek("Week 1");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 2")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.ChestAbs)
                {
                    exercise.ChangeWeek("Week 2");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 3")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.ChestAbs)
                {
                    exercise.ChangeWeek("Week 3");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }

            else if (week == "Week 4")
            {
                _listViewWorkouts.Items.Clear();
                foreach (Exercise exercise in _workout.ChestAbs)
                {
                    exercise.ChangeWeek("Week 4");
                    _listViewWorkouts.Items.Add(exercise);
                }
            }
        }

        /// <summary>
        /// Method to tell what week the user selected
        /// </summary>
        /// <param name="comboBox">Filled with week 1-4</param>
        /// <param name="day">The different combo boxes represent a different day, so if user makes week selection
        /// on _comboBoxChest, day 1 is sent to this method</param>
        /// <returns>The Week Selected in String</returns>
        private string WeekSelected(ComboBox comboBox, int day)
        {
            if (comboBox.SelectedIndex == 1)
            {
                _weekSelected = 1;
                _daySelected = day;
                return "Week 1";
            }

            else if (comboBox.SelectedIndex == 2)
            {
                _weekSelected = 2;
                _daySelected = day;
                return "Week 2";
            }

            else if (comboBox.SelectedIndex == 3)
            {
                _weekSelected = 3;
                _daySelected = day;
                return "Week 3";
            }

            else if (comboBox.SelectedIndex == 4)
            {
                _weekSelected = 4;
                _daySelected = day;
                return "Week 4";
            }

            else
            {
                return "";
            }
        }

        /// <summary>
        /// Event handler for when the user clicks to complete a workout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnButtonCompletedClick(object sender, RoutedEventArgs e)
        {
            //List add checks if the user has completed the correct workout, else it doesn't add to the list
            //(found on the profile page)
            ListAdd();

            //Navigate away from the workoutPage and send the _profile object with it
            Frame.Navigate(typeof(DashboardPage), _profile);
        }

        /// <summary>
        /// Mehtod to save updated list with workout completed information and the _day and _week data for the profile class
        /// </summary>
        private void Save()
        {
            using (FileStream profileStream = new FileStream(profileFilePath, FileMode.Create))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<string>));
                serializer.WriteObject(profileStream, _profile.List);
            }

            using (FileStream profileStream = new FileStream(profileFilePath2, FileMode.Create))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(Profile));
                serializer.WriteObject(profileStream, _profile);
            }
        }

        /// <summary>
        /// Adds to list found in profile page if the workout completed is the correct selected day and week
        /// Message Dialongs pop up to indicated to the user if the completion was successfull or not
        /// </summary>
        private async void ListAdd()
        {
            if (_profile.Week == 0)
            {
                _profile.Week = 1;
                _profile.Day = 1;
            }

            if (_weekSelected == _profile.Week && _daySelected == _profile.Day)
            {
                if (_profile.Day == 5)
                {
                    _profile.Week++;
                    _profile.Day = 1;
                    _profile.List.Add($"Week {_profile.Week}, Day {_profile.Day}");
                    MessageDialog msgDlg = new MessageDialog($"Good job!! You have completed Week {_profile.Week - 1}, Day 5 :)", "Workout Done!");
                    await msgDlg.ShowAsync();
                }
                else
                {
                    _profile.List.Add($"Week {_profile.Week}, Day {_profile.Day + 1}");
                    MessageDialog msgDlg = new MessageDialog($"Good job!! You have completed Week {_profile.Week}, Day {_profile.Day} :)", "Workout Done!");
                    await msgDlg.ShowAsync();
                    _profile.Day++;
                }
            }

            else
            {
                if (_profile.Week != 5)
                {
                    MessageDialog msgDlg = new MessageDialog($"You must complete Week {_profile.Week}, Day {_profile.Day}", "Uh Oh...");
                    await msgDlg.ShowAsync();
                }
            }

            Save();
        }
    } 
    #endregion

}
